from random import random
from general_test import general_test
import math
import itertools
from numpy import mean
from scipy.stats import chisquare

class overlapping_permutations_test(general_test):

    """
    Implementation of the overlapping permutations test
    """
    def rand_perm(self, n = 5):
        """
        returns a random permutation of n elements
        for the DIEHARD suites, n=5 is typically used
        """
        x = range(n)
        for i in reversed(x):
            j = int(math.floor(i*random()))
            x[j], x[i] = x[i], x[j]

        return x

    """
    tests for rand_perm, specifically
    """
    def test_rand_perm_len(self):
        assert len(self.rand_perm(5)) == 5

    def test_rand_perm_nothing_lost(self):
        for i in range(5):
            assert i in self.rand_perm(5)

    """
    utilities for actual overlapping permuatations test
    """

    def generate_permutations(self, n):
        if int(n) <= 0:
            raise ValueError("Must pass positive integer")

        return [self.rand_perm() for i in range(n)] 

    def test_len_generate_permutations(self):
        assert len(self.generate_permutations(10)) == 10  

    def tet_diff_permutations(self):
        p1 = self.generate_permutations(10)
        p2 = self.generate_permutations(10)
        assert p1 != p2

    def test_type_generate_permutations(self):
        assert type(self.generate_permutations(10)) == list

    def test_type_first_element_generate_permutations(self):
        assert type(self.generate_permutations(10)[0]) == list

    def get_frequency_of_permutation_data(self, perms):
        results = []

        for p in list(itertools.permutations(range(5))):
            i = 0
            for q in perms:
                if q == list(p):
                    i = i + 1
            results.append(i)
    
        return results

    def test_frequency_len(self):
        assert len(self.get_frequency_of_permutation_data(self.generate_permutations(42))) == 120

    def test_frequency_type(self):
        assert type(self.get_frequency_of_permutation_data(self.generate_permutations(5))) == list

    def test_frequency_first_element_type(self):
        assert type(self.get_frequency_of_permutation_data(self.generate_permutations(5))[0]) == int

    def test_frequency_actual_freq(self):
        assert sum(self.get_frequency_of_permutation_data(self.generate_permutations(42))) == 42

    def test_frequency_diff_vals(self):
        f1 = self.get_frequency_of_permutation_data(self.generate_permutations(5))
        f2 = self.get_frequency_of_permutation_data(self.generate_permutations(5))
        assert f1 != f2

    """
    now we do the actual test of randomness - confirm that the distribution of permutation freqs
    is uniform. Do some basic checks to see what's going on
    """
    def normalize(self, l):
        return [float(i)/sum(l) for i in l]

    def test_normalize(self):
        l = self.get_frequency_of_permutation_data(self.generate_permutations(1000))
        assert round(sum(self.normalize(l)), 5) == 1.00000

    def test_freq_has_valid_mean(self):
        l = self.get_frequency_of_permutation_data(self.generate_permutations(100))
        assert 0.001 < mean(self.normalize(l)) < 0.02

    """
    chi-squared test of fit for a normal distribution - the main event
    """
    def test_uniformity(self):
        n = 50
        c = self.normalize(self.get_frequency_of_permutation_data(self.generate_permutations(n))) 
        chi, p = chisquare(c, n-1)
        assert p < 0.01 
