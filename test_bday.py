from random import random
from general_test import general_test
from collections import Counter
import math

class birthday_spacing_test(general_test):

    """
    Implementation of the birthday spacing randomness test
    """
    n_size = 16777216 # 2^24, default interval size
    m_bdays = 1024 # 2^10, default no. of birthdays  

    """
    basic tests of generating the total interval
    """
    def get_year(self, n = n_size):
        return range(n)

    def test_year_length(self):
        assert len(self.get_year(42)) == 42

    def test_year_default(self):
        assert len(self.get_year()) == self.n_size

    """
    generate the random selection of birthdays, that is a subset of the year
    """

    def get_birthdays(self, m = m_bdays):
        x = self.get_year()
        for i in reversed(x):
            j = int(math.floor(i*random()))
            x[j], x[i] = x[i], x[j]

        return x[:m]

    def test_birthdays_length(self):
        assert len(self.get_birthdays(42)) == 42
       
    def test_birthdays_default(self):
        assert len(self.get_birthdays()) == self.m_bdays

    def test_year_ranges(self):
        assert len(self.get_year()) > len(self.get_birthdays()) 

    def test_birthdays_is_subset_of_length(self):
        s = set(self.get_birthdays())
        t = set(self.get_year())
        assert s.issubset(t)

    def get_intervals(self):
        bdays = self.get_birthdays()
        bdays.sort()
        intervals = []
        for i in range(len(bdays)-1):
            intervals.append(bdays[i+1] - bdays[i])
            
        return intervals 

    def test_all_intervals_positive(self):
        assert all(i > 0 for i in self.get_intervals())

    def get_interval_frequencies(self, intervals):
        """
		only return interval values that occur more than once
		"""
        counter = Counter(intervals)
        return [val for val in counter.values() if val > 1]

    """
	finally, check that the intervals have the correct Poisson distribution
	"""
    def test_intervals_are_poisson(self):
        assert False