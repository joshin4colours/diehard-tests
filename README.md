This project contains implementations of the DIEHARD tests for a random number generator (see http://stat.fsu.edu/pub/diehard/ for more details). These tests currently include

- Overlapping permuatations test
- Birthday spacings test

with the intention of implementing all individual tests found in the original diehard test suite. 

This project is a personal one. The random number generator under test is the random.random generator in Python. 